% Copyright CNRS/Inria/UCA
% Contributor(s): Eric Debreuve (since 2021)
%
% eric.debreuve@cnrs.fr
%
% This software is governed by the CeCILL  license under French law and
% abiding by the rules of distribution of free software.  You can  use,
% modify and/ or redistribute the software under the terms of the CeCILL
% license as circulated by CEA, CNRS and INRIA at the following URL
% "http://www.cecill.info".
%
% As a counterpart to the access to the source code and  rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty  and the software's author,  the holder of the
% economic rights,  and the successive licensors  have only  limited
% liability.
%
% In this respect, the user's attention is drawn to the risks associated
% with loading,  using,  modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean  that it is complicated to manipulate,  and  that  also
% therefore means  that it is reserved for developers  and  experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or
% data to be ensured and,  more generally, to use and operate it in the
% same conditions as regards security.
%
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL license and that you accept its terms.

% Convert to html with: latex2html -nonavigation -split +0 main.tex
% or: make4ht --config main.cfg --output-dir main-tex4ht main.tex
%
\documentclass[11pt]{article}

\newcommand{\packagetitle}{Cell Tracking BC\\Base Classes for Cell Tracking in Microscopy}
\newcommand{\packagename}{\texttt{cell\_tracking\_BC}}


%\usepackage[default]{cantarell}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage[a4paper,top=25mm,bottom=25mm]{geometry}

\usepackage{lastpage}
\usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyhead{}
    \fancyhead[LO,CE]{\packagename}
    \fancyhead[RO,LE]{\leftmark}
    \fancyfoot{}
    %\fancyfoot[LO,CE]{Left}
    %\fancyfoot[CO,RE]{Center}
    \fancyfoot[LE,RO]{\thepage{\footnotesize\raisebox{0.2em}{$/$}}\pageref{LastPage}}

\usepackage{tabto}
%\usepackage{enumitem}
%    \newcommand{\compactlist}{noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt}
\usepackage[dvipsnames]{xcolor}
    \definecolor{darkgreen}{HTML}{009900}

\usepackage{hyperref}
    \hypersetup{
        colorlinks=true,
        linkcolor=Brown,
        urlcolor=Brown,
        pdftitle={\packagetitle},
    }
\usepackage[capitalise]{cleveref} % Must be loaded after hyperref


\newcommand{\mod}[1]{\texttt{\textcolor{darkgreen}{#1}}}
\newcommand{\cls}[1]{\texttt{\textcolor{blue}{#1}}}
\newcommand{\fct}[1]{\texttt{\textcolor{Cerulean}{#1}}}
\newcommand{\prm}[1]{\texttt{\textcolor{red}{#1}}}
\newcommand{\oprm}[2]{\texttt{\textcolor{magenta}{#1}=\textcolor{magenta}{#2}}}
\newcommand{\oprmv}[1]{\texttt{\textcolor{magenta}{#1}}}


\title{\packagetitle}
\author{Eric Debreuve\\
    \small{%
        \href{https://www.cnrs.fr/index.html}{CNRS} employee,
        Team \href{https://team.inria.fr/morpheme}{Morpheme},
        \href{https://univ-cotedazur.fr/en}{University Nice Côte d'Azur (UCA)}}%
    }
\date{Since 2021}


\begin{document}
    \maketitle

    \section{Purpose}

    The package \packagename{} provides bases classes to develop applications of cell tracking in microscopy.
    The main base classes cover nucleus, cytoplasm, cell, frame, sequence, and tracks.
    Typical applications can be developed by simply writing specific functions that are passed to methods of the base classes.
    Applications requiring a deeper specialization will probably need to implement new classes inheriting from the provided base ones.


    \section{Thanks}

    \begin{itemize}
        \item The project is developed with \href{https://www.jetbrains.com/pycharm}{PyCharm Community}.
        \item The code is formatted by \href{https://github.com/psf/black}{Black}, \textit{The Uncompromising Code Formatter}.
        \item The imports are ordered by \href{https://github.com/timothycrosley/isort}{isort}\dots \textit{your imports, so you don't have to}.
        \item Moreover, the project relies on several Open Source Python packages.
    \end{itemize}


    \section{Installation}

    \packagename{} is available on the \href{https://pypi.org}{Python Package Index (PyPI)} repository at:\newline
    \href{https://pypi.org/project/cell-tracking-bc}{pypi.org/project/cell-tracking-bc}.
    It requires Python 3.8 or higher.
    It can be installed and updated as follows:
    %
    \begin{itemize}
        \item For all users, after acquiring administrative rights:
        \begin{itemize}
            \item First installation:\tabto{0.3\linewidth}\texttt{pip3 install cell-tracking-bc}
            \item Installation update:\tabto{0.3\linewidth}\texttt{pip3 install --upgrade cell-tracking-bc}
        \end{itemize}
        \item For the current user (no administrative rights required):
        \begin{itemize}
            \item First installation:\tabto{0.3\linewidth}\texttt{pip3 install --user cell-tracking-bc}
            \item Installation update:\tabto{0.3\linewidth}\texttt{pip3 install --user --upgrade cell-tracking-bc}
        \end{itemize}
    \end{itemize}
    %
    It should also be installable using Python distribution managers such as \href{https://www.anaconda.com/products/individual}{Anaconda}, or from within Integrated Development Environments (IDEs) such as \href{https://www.jetbrains.com/pycharm}{PyCharm}.\newline
    \newline
    \textit{Note:} The command \texttt{pip3} was used above to emphasize that \packagename{} requires major version 3 of Python.
    If \texttt{pip} defaults to this version, it can of course be used instead.


    \section{Common Remarks}

    \subsection{Parameter \prm{channel}}\label{sec:pa-ch}

    When a function or method has a (mandatory or optional) parameter \prm{channel} to limit its application to some specific channels of the sequence, it can be either a single channel name, or a list or tuple of channel names (unless otherwise stated), for example \texttt{"GFP"} or \texttt{("GFP", "Dapi")}.
    If optional and omitted, it usually defaults to the base channels of the sequence (see \cref{sec:cl-se}).


    \subsection{Parameter \oprmv{show\_and\_wait}}\label{sec:pa-sh-an-wa}

    The function or method parameter \oprmv{show\_and\_wait}, usually optional, is a boolean stating whether the figure(s) the function or method is about to create should be displayed immediately and pause the application execution until the figure(s) is/are closed, or only prepared for a future display.
    It is useful to prepare several figures in a row, and then show them all at once, either by letting the last call to such a function or method setting \oprmv{show\_and\_wait} to True, or by calling \mod{matplotlib.pyplot}.\fct{show}() for example.
    This parameter usually defaults to True.


    \section{Main Base Classes}

    \subsection{Class \texttt{sequence\_t}}\label{sec:cl-se}

    The class \cls{sequence\_t} is meant to serve as the main entry point of \packagename.
    It represents a sequence of single or multi-channel frames and should define all the necessary attributes and methods for basic tracking applications.
    The channels loaded from the sequence file are referred to as ``base channels''.
    Other channels, computed from the base ones and/or previously computed ones, can be added at will.
    Collectively, they form the channels of the sequence.

    \subsection{Sequence How To's}

    \paragraph{Load a sequence:} \texttt{sequence = \cls{sequence\_t}.\fct{NewFromPath}(}\newline
    \texttt{\prm{path\_to\_file}, \prm{channels}, \oprm{first\_frame}{index\_1}, \oprm{last\_frame}{index\_2})}
    \newline
    \newline
    where
    %
    \begin{itemize}
        \item \cls{sequence\_t} is defined in \mod{cell\_tracking\_BC.type.sequence}
        \item \prm{channels} is a list or tuple of the names of the channels composing the sequence (see \cref{sec:pa-ch} for an example).
            Any name equal to None, "\_$\,$\_$\,$\_", or "-$\,$-$\,$-" will cause the corresponding channel to be skipped and will therefore not be part of the sequence base channels (see \cref{sec:cl-se}).
        \item \oprmv{index\_1} is the 0-based index of the first frame to be loaded (Optional; Defaults to 0)
        \item \oprmv{index\_2} is the 0-based index of the last frame to be loaded (Optional; Defaults to 999999)
    \end{itemize}


    \paragraph{Check a loaded sequence:} \texttt{\fct{ShowSequenceStatistics}(}\newline
    \texttt{\prm{sequence}, \oprm{channel}{channel(s)}, \oprm{show\_and\_wait}{boolean})}
    \newline
    \newline
    where
    %
    \begin{itemize}
        \item \fct{ShowSequenceStatistics} is defined in \mod{cell\_tracking\_BC.in\_out.screen.sequence}
        \item \prm{channel(s)}: See \cref{sec:pa-ch}
        \item \oprmv{show\_and\_wait}: See \cref{sec:pa-sh-an-wa}
    \end{itemize}


    \subsection{Class \texttt{cell\_t}}\label{sec:cl-ce}


    \subsection{Cell How To's}


    \section{Additional Base Classes}

    \texttt{compartment\_t}: It is the base class of \texttt{nucleus\_t}, \texttt{cytoplasm\_t}, and \texttt{cell\_t}.
    \texttt{archiver\_t}

\end{document}
